// Výběr elementů z DOM
const selectors = {
    boardContainer: document.querySelector('.board-container'),
    board: document.querySelector('.board'),
    moves: document.querySelector('.moves'),
    timer: document.querySelector('.timer'),
    start: document.querySelector('#start-btn'),
    win: document.querySelector('.win'),
    difficulty: document.querySelector('#difficulty'),
    playerMode: document.querySelector('#player-mode'),
    pcDifficulty: document.querySelector('#pc-difficulty'),
    theme: document.querySelector('#theme'),
    player1Score: document.querySelector('.player1-score'),
    player2Score: document.querySelector('.player2-score')
}

// Stav hry
const state = {
    gameStarted: false,
    flippedCards: 0,
    totalFlips: 0,
    totalTime: 0,
    loop: null,
    playerMode: '2',
    pcDifficulty: 'dumb',
    theme: 'emojis',
    dimension: 4,
    matchedPairs: [],
    pcMemory: {},
    pcTurn: false,
    currentPlayer: 1,
    scores: {
        player1: 0,
        player2: 0
    }
}

// Zamíchání pole
const shuffle = array => {
    const clonedArray = [...array]
    for (let i = clonedArray.length - 1; i > 0; i--) {
        const randomIndex = Math.floor(Math.random() * (i + 1))
        const original = clonedArray[i]
        clonedArray[i] = clonedArray[randomIndex]
        clonedArray[randomIndex] = original
    }
    return clonedArray
}

// Náhodný výběr položek z pole
const pickRandom = (array, items) => {
    const clonedArray = [...array]
    const randomPicks = []
    for (let i = 0; i < items; i++) {
        const randomIndex = Math.floor(Math.random() * clonedArray.length)
        randomPicks.push(clonedArray[randomIndex])
        clonedArray.splice(randomIndex, 1)
    }
    return randomPicks
}

// Generování hry
const generateGame = () => {
    state.dimension = parseInt(selectors.difficulty.value)
    state.theme = selectors.theme.value

    const emojis = ['🥔', '🍒', '🥑', '🌽', '🥕', '🍇', '🍉', '🍌', '🥭', '🍍', '😀', '😁', '😂', '😇', '😈', '😍', '😎', '😆', '😑', '😒', '😭', '😷', '🤑', '🤢', '🥶', '🙈', '💀', '🤬', '👽', '💩', '🚀', '🌍', '🌔', '👊', '💪']
    const numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35']

    const items = state.theme === 'emojis' ? emojis : numbers

    const picks = pickRandom(items, (state.dimension * state.dimension) / 2)
    const shuffledItems = shuffle([...picks, ...picks])
    const cards = `
        <div class="board" style="grid-template-columns: repeat(${state.dimension}, auto)">
            ${shuffledItems.map(item => `
                <div class="card">
                    <div class="card-front"></div>
                    <div class="card-back">${item}</div>
                </div>
            `).join('')}
        </div>
    `

    const parser = new DOMParser().parseFromString(cards, 'text/html')
    selectors.boardContainer.innerHTML = '' // Vymazání herní plochy
    selectors.boardContainer.appendChild(parser.querySelector('.board'))
}

// Spuštění hry
const startGame = () => {
    state.gameStarted = true
    state.totalTime = 0
    state.totalFlips = 0
    state.flippedCards = 0
    state.matchedPairs = []
    state.pcMemory = {}
    state.pcTurn = false
    state.currentPlayer = 1
    state.scores.player1 = 0
    state.scores.player2 = 0
    selectors.moves.innerText = '0 tahy'
    selectors.timer.innerText = 'Čas: 0 s'
    selectors.start.classList.add('disabled')
    selectors.win.classList.remove('show')
    selectors.boardContainer.classList.remove('flipped')
    selectors.player1Score.innerText = 'Hráč 1: 0'
    selectors.player2Score.innerText = 'Hráč 2: 0'

    state.loop = setInterval(() => {
        state.totalTime++
        selectors.moves.innerText = `${state.totalFlips} tahy`
        selectors.timer.innerText = `Čas: ${state.totalTime} s`
    }, 1000)
}

// Otočení karet zpět
const flipBackCards = () => {
    document.querySelectorAll('.card:not(.matched)').forEach(card => {
        card.classList.remove('flipped')
    })
    state.flippedCards = 0
}

// Otočení karty
const flipCard = (card, force = false) => {
    if (!force && state.pcTurn) return

    state.flippedCards++
    state.totalFlips++

    if (!state.gameStarted) {
        startGame()
    }

    if (state.flippedCards <= 2) {
        card.classList.add('flipped')
    }

    if (state.flippedCards === 2) {
        const flippedCards = document.querySelectorAll('.flipped:not(.matched)')
        const card1 = flippedCards[0]
        const card2 = flippedCards[1]

        if (card1.innerText === card2.innerText) {
            card1.classList.add('matched')
            card2.classList.add('matched')
            state.matchedPairs.push(card1.innerText)
            if (state.currentPlayer === 1) {
                state.scores.player1++
                selectors.player1Score.innerText = `Hráč 1: ${state.scores.player1}`
            } else {
                state.scores.player2++
                selectors.player2Score.innerText = `Hráč 2: ${state.scores.player2}`
            }
        } else {
            setTimeout(() => {
                flipBackCards()
                state.currentPlayer = state.currentPlayer === 1 ? 2 : 1
            }, 1000)
        }
    }

    if (!document.querySelectorAll('.card:not(.flipped)').length) {
        setTimeout(() => {
            selectors.boardContainer.classList.add('flipped')
            selectors.win.innerHTML = `
                <span class="win-text">
                    Vyhrál jsi!<br />
                    s <span class="highlight">${state.totalFlips}</span> tahy<br />
                    za <span class="highlight">${state.totalTime}</span> sekund
                </span>
            `
            selectors.win.classList.add('show')
            clearInterval(state.loop)
        }, 1000)
    }

    if (state.flippedCards === 2) {
        setTimeout(() => {
            state.flippedCards = 0
            if (state.playerMode === 'pc' && !document.querySelectorAll('.card:not(.flipped)').length) {
                pcMove()
            }
        }, 1000)
    }
}

// Tah PC
const pcMove = () => {
    state.pcTurn = true

    const unmatchedCards = Array.from(document.querySelectorAll('.card:not(.matched):not(.flipped)'))
    const randomCard = unmatchedCards[Math.floor(Math.random() * unmatchedCards.length)]

    flipCard(randomCard, true)

    setTimeout(() => {
        const secondUnmatchedCards = Array.from(document.querySelectorAll('.card:not(.matched):not(.flipped)'))
        const secondRandomCard = secondUnmatchedCards[Math.floor(Math.random() * secondUnmatchedCards.length)]
        flipCard(secondRandomCard, true)
        state.pcTurn = false
    }, 1000)
}

// Přidání posluchačů událostí
const attachEventListeners = () => {
    document.addEventListener('click', event => {
        const eventTarget = event.target
        const eventParent = eventTarget.parentElement

        if (eventTarget.className.includes('card') && !eventParent.className.includes('flipped')) {
            flipCard(eventParent)
        } else if (eventTarget.id === 'start-btn' && !eventTarget.className.includes('disabled')) {
            startGame()
        }
    })

    selectors.playerMode.addEventListener('change', event => {
        const mode = event.target.value
        state.playerMode = mode
        selectors.pcDifficulty.disabled = mode !== 'pc'
    })

    selectors.difficulty.addEventListener('change', generateGame)
    selectors.theme.addEventListener('change', generateGame)
}

generateGame()
attachEventListeners()
